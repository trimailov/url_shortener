module gitlab.com/trimailov/url_shortener

go 1.12

require (
	github.com/gorilla/mux v1.7.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
)
