FROM golang:1.12-alpine

RUN apk add --update git

WORKDIR /go/url_shortener
COPY . .

RUN go get ./

CMD go run main.go

EXPOSE 8080
