CREATE TABLE urls (
    id bigserial PRIMARY KEY,
    url_key text UNIQUE NOT NULL,
    full_url text NOT NULL,
    full_url_check_sum text UNIQUE NOT NULL
);

CREATE INDEX ON urls (full_url_check_sum);
CREATE INDEX ON urls (url_key);
