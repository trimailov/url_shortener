URL shortener
=============

Excercise in Go web dev

Development
-----------

Build:

```
docker-compose pull
docker-compose build
docker-compose up
```

Run commands on app container:

```
docker-compose exec app <COMMAND>
```

Open postgres terminal:

```
docker-compose exec db psql -U admin url_shortener
```
