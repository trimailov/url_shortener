package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var db *sqlx.DB

type Url struct {
	Id              uint64 `db:"id"`
	UrlKey          string `db:"url_key"`
	FullUrl         string `db:"full_url"`
	FullUrlCheckSum string `db:"full_url_check_sum"`
}

func main() {
	initDB()

	r := mux.NewRouter()

	r.HandleFunc("/{urlKey}", GetURL).Methods("GET")
	r.HandleFunc("/api/url/create", ApiCreateNewUrl).Methods("POST")

	http.ListenAndServe(":8080", r)
}

func initDB() {
	dataSourceName := "host=db port=5432 user=admin password=admin123 dbname=url_shortener sslmode=disable"
	pgDb, err := sqlx.Open("postgres", dataSourceName)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Print("Connect to DB SUCCESS!")
	}

	db = pgDb
}

func GetURL(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	urlKey := vars["urlKey"]

	var url Url

	err := db.Get(&url, "SELECT * FROM urls WHERE url_key=$1 LIMIT 1", urlKey)

	if err != nil {
		log.Println(err)
	}

	fmt.Fprintf(w, "URL is: %s\n", url.FullUrl)
}

func ApiCreateNewUrl(w http.ResponseWriter, r *http.Request) {
	var urlToSave Url

	newUrl := r.FormValue("newUrl")

	urlKey := GenerateKey()

	urlCheckSum := sha1.Sum([]byte(newUrl))
	urlCheckSumStr := hex.EncodeToString(urlCheckSum[:])

	urlToSave = Url{
		UrlKey:          urlKey,
		FullUrl:         newUrl,
		FullUrlCheckSum: urlCheckSumStr,
	}

	// err := db.Get(&urlToSave, "SELECT * FROM urls WHERE full_url_check_sum = $1 LIMIT 1", urlToSave.FullUrlCheckSum)

	_, err := db.Exec(
		"INSERT INTO urls (url_key, full_url, full_url_check_sum) VALUES ($1, $2, $3)",
		urlToSave.UrlKey,
		urlToSave.FullUrl,
		urlToSave.FullUrlCheckSum,
	)
	if err != nil {
		log.Fatal(err)
	}

    fmt.Fprintf(w, "New Url is: http://localhost:8080/%s\n", urlToSave.UrlKey)
}

func GenerateKey() string {
	var randArray [8]string
	const KeyLength = 8

	digitArray := [62]string{
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
		"k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
		"u", "v", "w", "x", "y", "z",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
		"U", "V", "W", "X", "Y", "Z"}

	for i := 0; i < 8; i++ {
		t := time.Now().UnixNano()
		rand.Seed(t)
		randArray[i] = digitArray[rand.Intn(62)]
	}

	return strings.Join(randArray[:], "")
}
